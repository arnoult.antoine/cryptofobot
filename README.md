# Project

CFB is a bot for faucets

## Installation

Copy dotenv file and fill MAIL and PASS with your credentials
```bash
cp ./app/env.sample ./app/.env
```

### Docker

Build the docker image and run it

```bash
docker build -t cryptofobot:1 .
docker run cryptofobot:1 node cfb.js
```

### Command line

You can run the script in command line but be sure to install node and all required dependencies for using the bot. Once all is done, you can run the bot with the following command

```bash
node cfb.js
```

### UI [WIP]

I think it can be interesting to make an UI for the bot

## Configuration

Bot can be configured through 2 files :
 - [app/config/websites.json](app/config/websites.json) where you can choose which site to enable or disable
 - [app/config/promoCode.txt](app/config/promoCode.txt) where you can define the bonus roll

ps : Do not touch lastCall file except if you know what you are doing

## Support

If you appreciate the work and in order to support the project please use referral link to create account

To support this project please register on Cryptos Faucets websites using my referral links and use the bot often. You can also share these links and the bot with other people:

ETH : https://freeethereum.com/?ref=107158

BTC : https://freebitcoin.io/?ref=376363

LTC : https://free-ltc.com/?ref=38234

BNB : https://freebinancecoin.com/?ref=77687

DASH : https://freedash.io/?ref=59475

ADA : https://freecardano.com/?ref=253723

USDT : https://freetether.com/?ref=119712

USDC : https://freeusdcoin.com/?ref=80753

TRX : https://free-tron.com/?ref=111513

XEM : https://freenem.com/?ref=255567

LINK : https://freechainlink.io/?ref=38912

NEO : https://freeneo.io/?ref=47191

XRP : https://coinfaucet.io/?ref=721356

## Thanks

Huge inspiration comes from https://github.com/renowator/CryptosFaucets_Bot

## License

The source code for the project is licensed under the MIT license, which you can find in the LICENSE file.

