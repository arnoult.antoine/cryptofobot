require('dotenv').config()

const fs = require('fs').promises;
const select = require ('puppeteer-select')
const puppeteer = require('puppeteer-extra')
// add stealth plugin and use defaults (all evasion techniques)
const StealthPlugin = require('puppeteer-extra-plugin-stealth')
puppeteer.use(StealthPlugin())

const TIMEOUT = 45000
const ONE_HOUR_MS = 3600000
const LAST_CALL_FS = 'lastCall'
const WEBSITES_FS = 'config/websites.json'
const PROMOCODE_FS = 'config/promoCode.txt'
const UA = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36'
const MAX_ERROR_PERCENT = 30

let currentSleep = -1
let isRunning = true


/**
 * Scroll the page. We need to scroll in order to allow to click on element.Element is accessible only if it
 * is visible and it can be visible with scroll
 *
 * @param page
 * @return {Promise<void>}
 */
const autoScroll = async (page) => {
  await page.evaluate(async () => {
    await new Promise((resolve) => {
      let totalHeight = 0
      const distance = 50
      const timer = setInterval(() => {
        const scrollHeight = document.body.scrollHeight
        window.scrollBy(0, distance)
        totalHeight += distance

        if (totalHeight >= scrollHeight) {
          clearInterval(timer)
          resolve()
        }
      }, 100)
    })
  })
}

const sleep = (ms) => {
  return new Promise(resolve => {
    currentSleep = setTimeout(resolve, ms)
  })
}

/**
 * Save timestamp to know the last time we opened faucet with success
 */
const saveTimestamp = async (date) => {
  try {
    await fs.writeFile(LAST_CALL_FS, date)
  } catch (e) {
    console.error('Writing Timestamp error', e)
  }
}

/**
 * Get last timestamp where we opened successfully faucet
 */
const getTimestamp = async () => {
  try {
    return await fs.readFile(LAST_CALL_FS, 'utf8')
  } catch (e) {
    // console.error('Reading Timestamp error', e)
    return 0
  }
}

/**
 * Search the timestamp when we must open faucet
 */
const getWakeUpTs = async() => {
  try {
    const data = await fs.readFile(LAST_CALL_FS, 'utf8')
    const now = Date.now()
    return (now - data > ONE_HOUR_MS) ? ONE_HOUR_MS : ONE_HOUR_MS - (now - data)
  } catch (e) {
    // console.error('Timestamp error', e)
    return ONE_HOUR_MS
  }
}

const dateToPrint = () => {
  const now = new Date();
  return '[' + now.getHours() + 'h' + ( (now.getMinutes() < 10 ? '0' : '') + now.getMinutes() ) + '] '
}

/**
 * Add random offset between #start and #end. The aim of this function is to
 * add some random between actions to avoid suspicion
 *
 * @return {number} between 1 second and 5 minutes
 */
const randomOffset = () => {
  const start = 1000  // 1 second
  const end =  300000  // 5 minutes
  return random(start, end)
}

const random = (start, end) => Math.floor(Math.random() * (end - start + 1) + start)

const duringToPrint = (delayInMs, offsetInMs = 0) => {
  const second = 1000
  const minute = second * 60
  const hour = minute * 60

  let hours = Math.floor(delayInMs / hour % 24)
  let minutes = Math.floor(delayInMs / minute % 60)
  let seconds = Math.floor(delayInMs / second % 60)

  let offsetMsg = ''
  if (offsetInMs > 0) {
    let minutesOffset = Math.floor(offsetInMs / minute % 60)
    let secondsOffset = Math.floor(offsetInMs / second % 60)

    offsetMsg = '[including offset=' + (minutesOffset > 0 ? minutesOffset + 'm ' : '') + secondsOffset + 's]'
  }
  return (hours > 0 ? hours + ' h ' : '') +
      (minutes > 0 ? minutes + ' min ' : '') +
      seconds + ' seconds ' + offsetMsg
}

const logMsg = (msg, erase = false) => {
  console.log(msg)
  if (typeof document !== 'undefined') {
    if (erase) {
      document.querySelector('#logger').innerHTML = msg
    } else {
      document.querySelector('#logger').innerHTML += msg
    }
    document.querySelector('#logger').innerHTML += '<br/>'
  }
}

const stop = () => {
  isRunning = false
  clearTimeout(currentSleep)
  logMsg('Bot has been interrupted', true)
  if (typeof document !== 'undefined') {
    document.querySelector('#run').disabled = false
    document.querySelector('#stop').disabled = true
  }
}

/**
 * Here are all steps in order to roll
 *
 * @param page this is a Puppeteer.Browser
 * @param url string for the url
 * @param promoCode this is the string for promoCode, empty if no promoCode
 * @return {Promise<void>}
 */
const roll = async (page, url, promoCode = '') => {
  const email = process.env.EMAIL
  const pass = process.env.PASS

  await page.goto(url, { waitUntil: 'networkidle2', timeout: TIMEOUT })
  await sleep(random(7000, 12000))  // Avoid cloudflare 5 seconds redirect
  await page.waitForSelector('.login-wrapper input[name=email]', { visible: true })
  logMsg(url)

  await page.type('input[name=email]', email, { delay: 20 })
  await page.type('input[name=password]', pass, { delay: 20 })
  try {
    await page.evaluate(() => document.querySelector('button.login').scrollIntoView({ behavior: 'smooth' }))
    await page.click('button.login')
  } catch (e) {
    logMsg('Can\'t click on login, trying another technique ⚠️')
    const element = await select(page).getElement('button:contains(LOGIN!)')
    await element.click()
  }
  // To avoid timeout -> use networkidle2
  await page.waitForNavigation({ waitUntil: 'networkidle2', timeout: TIMEOUT })
  logMsg('   Login ok...')
  try {
    await rolling(page)
  } catch (e) {
    logMsg('Trouble with rolling')
    throw e
  }
  if (promoCode) {
    logMsg('-------- ATTEMPTING BONUS ROLLS --------')
    await page.waitForSelector('.input-group input[name=hash]', { visible: true })
    await page.type('input[name=hash]', promoCode)
    await page.evaluate(() => document.querySelector('button.submit-promo').scrollIntoView({ behavior: 'smooth' }))
    await page.click('button.submit-promo')
    // await page.waitForNavigation({ waitUntil: 'networkidle2', timeout: TIMEOUT })
    await sleep(random(5000, 8000))  // Avoid cloudflare 5 seconds redirect
    await page.goto(url, { waitUntil: 'networkidle2', timeout: TIMEOUT })
    logMsg('💸 Bonus for ' + url)

    try {
      await rolling(page)
    } catch (e) {
      logMsg("  👎 Bonus coins troubles. PromoCode has been removed\n\n")
      await removePromoCode()
      throw e
    }
  }
}

/**
 * This is action to click on Roll button
 * @param page page where is the button roll
 * @return {Promise<void>}
 */
const rolling = async(page) => {
  await autoScroll(page)
  try {
    // sometimes querySelector is null
    await page.evaluate(() => document.querySelector('button.roll-button').scrollIntoView({ behavior: 'smooth' }))
    await page.click('button.roll-button')
  } catch (e) {
    try {
      logMsg('Can\'t click on roll, trying another technique ⚠️')
      const elementRoll = await select(page).getElement('button:contains(ROLL!)')
      await elementRoll.click()
    } catch (e) {
      logMsg('Throw error while rolling')
      logMsg('Error: ' + e.message)
      throw e
    }
  }
  await sleep(random(2000, 7000))
  // Get inner HTML
  try {
    const innerText = await page.evaluate(() => document.querySelector('.navbar-coins').innerText)
    logMsg('Balance 🏛️ -> ' + innerText)
  } catch (e) {
    logMsg('Balance 🏛️ -> impossible to retrieve balance')
  }
  logMsg("   👍 SUCCESS! Coin claimed!\n\n")
}

const removePromoCode = async () => {
  try {
    await fs.writeFile(PROMOCODE_FS, '')
  } catch (e) {
    logMsg('❌ Impossible to remove promoCode')
  }
}


/**
 * MAIN
 */
const runBot = async () => {
  logMsg('🦾 ----------------- BOT STARTING ----------------- 🦾', true)
  logMsg('Use email: ' + process.env.EMAIL)
  logMsg("\n  🍀 Good Luck!\n")


  while (isRunning) {  // Daemon

    const websitesFs = await fs.readFile(WEBSITES_FS, 'utf8')
    const websites = JSON.parse(websitesFs).filter(site => site.isActive)

    const nowTs = Date.now()
    const lastCallTs = await getTimestamp()

    if (websites.length === 0) {
      logMsg('It seems there is no active site. Please enable at least one isActive property')
      isRunning = false
      break
    }

    if ((nowTs - lastCallTs) > ONE_HOUR_MS) {
      let nbError = 0

      const browser = await puppeteer.launch({ headless: true, ignoreHTTPSErrors: true, args: [
          '--disable-setuid-sandbox',
          '--no-sandbox',
          '--user-agent=' + UA,
          '--lang=en'
        ]
      })
      const page = await browser.newPage()
      await page.setJavaScriptEnabled(true)
      await page.setUserAgent(UA)
      await page.setViewport({ width: 1866, height: 768})

      const promoCodeFs = await fs.readFile(PROMOCODE_FS, 'utf8')
      logMsg(dateToPrint() + '----------------- ATTEMPTING ROLLS -----------------')
      for (let i = 0; i < websites.length; i++) {
        try {
          await roll(page, websites[i].url, promoCodeFs)
        } catch(e) {
          logMsg('Error was encountered on: ' + websites[i].url)
          logMsg("Error: " + e.message)
          logMsg("  👎 FAIL. Coin not claimed. ❌\n\n")
          try {
            await page.screenshot({path: 'error-screen-' + websites[i].cryptoCode + '.png', fullPage: true})
          } catch (e) {
            logMsg('Error to take a screenshot on: ' + websites[i].url)
          }
          nbError++
        }
      }

      // Remove promo code
      await removePromoCode()

      await page.close()
      await browser.close()
      if (nbError > 0) {
        const errorPercent = Math.floor((nbError / websites.length) * 100)

        if (errorPercent < MAX_ERROR_PERCENT) {
          logMsg('🥺 Some error have been detected but we continue because it is lower than ' + MAX_ERROR_PERCENT + '%')

          await saveTimestamp(Date.now().toString())
          await sleep(ONE_HOUR_MS + randomOffset())
        } else {
          logMsg(Date.now().toString())
          logMsg('An error has been detected during exe. Please manually check. Stop running script ! ❌❌')
          break
        }
      } else {
        logMsg('🏆 All coins have been collected successfully! Well job')
        logMsg(dateToPrint() + "Going to sleep\n   See you in one hour 💤")

        await saveTimestamp(Date.now().toString())
        await sleep(ONE_HOUR_MS + randomOffset())
        logMsg('----------------- WAKE UP -----------------')
      }

    } else {
      const offset = randomOffset()
      let delay = await getWakeUpTs()

      logMsg(dateToPrint() + "💤 I'm sleeping\n   See you in " + duringToPrint(delay, offset) + ' 💤')

      delay += offset
      await sleep(delay)
      logMsg('----------------- WAKE UP -----------------')
    }

  }

  logMsg('Bot has been interrupted')
}


module.exports.runBot = runBot
module.exports.stop = stop
