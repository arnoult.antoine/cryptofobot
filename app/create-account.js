require('dotenv').config()

const fs = require('fs').promises;
const select = require ('puppeteer-select')
const puppeteer = require('puppeteer')

const ONE_HOUR_MS = 3600000
const LAST_CALL_FS = 'lastCall'
const WEBSITES_FS = 'config/websites.txt'

/**
 * Scroll the page. We need to scroll in order to allow to click on element.Element is accessible only if it
 * is visible and it can be visible with scroll
 *
 * @param page
 * @return {Promise<void>}
 */
const autoScroll = async (page) => {
  await page.evaluate(async () => {
    await new Promise((resolve) => {
      let totalHeight = 0
      const distance = 50
      const timer = setInterval(() => {
        const scrollHeight = document.body.scrollHeight
        window.scrollBy(0, distance)
        totalHeight += distance

        if (totalHeight >= scrollHeight) {
          clearInterval(timer)
          resolve()
        }
      }, 100)
    })
  })
}

const sleep = (ms) => {
  return new Promise(resolve => setTimeout(resolve, ms))
}

/**
 * Save timestamp to know the last time we opened faucet with success
 */
const saveTimestamp = async (date) => {
  try {
    await fs.writeFile(LAST_CALL_FS, date)
  } catch (e) {
    console.error('Writing Timestamp error', e)
  }
}

/**
 * Get last timestamp where we opened successfully faucet
 */
const getTimestamp = async () => {
  try {
    return await fs.readFile(LAST_CALL_FS, 'utf8')
  } catch (e) {
    // console.error('Reading Timestamp error', e)
    return 0
  }
}

/**
 * Search the timestamp when we must open faucet
 */
const getWakeUpTs = async() => {
  try {
    const data = await fs.readFile(LAST_CALL_FS, 'utf8')
    const now = Date.now()
    return (now - data > ONE_HOUR_MS) ? ONE_HOUR_MS : ONE_HOUR_MS - (now - data)
  } catch (e) {
    // console.error('Timestamp error', e)
    return ONE_HOUR_MS
  }
}

const dateToPrint = () => {
  const now = new Date();
  return '[' + now.getHours() + 'h' + ( (now.getMinutes() < 10 ? '0' : '') + now.getMinutes() ) + '] '
}

/**
 * Add random offset between #start and #end. The aim of this function is to
 * add some random between actions to avoid suspicion
 *
 * @return {number} between 1 second and 5 minutes
 */
const randomOffset = () => {
  const start = 1000  // 1 second
  const end =  300000  // 5 minutes
  return Math.floor(Math.random() * (end - start + 1) + start)
}

const duringToPrint = (timeInMs, offsetInMs = 0) => {
  const timeDate = new Date(timeInMs + offsetInMs)
  let offsetMsg = ''
  if (offsetInMs > 0) {
    const offsetDate = new Date(offsetInMs)
    offsetMsg = '[including offset=' + (offsetDate.getMinutes() > 0 ? offsetDate.getMinutes() + 'm ' : '') +
        offsetDate.getSeconds() + 's]'
  }
  return (timeDate.getMinutes() > 0 ? timeDate.getMinutes() + ' min ' : '') + timeDate.getSeconds() + ' seconds ' + offsetMsg
}

/**
 * MAIN
 */
(async () => {
  const email = process.env.NEW_EMAIL
  const pass = process.env.NEW_PASS
  console.log('🦾 ----------------- CREATE ACCOUNT BOT ----------------- 🦾')
  console.log('Use email:', email)
  console.log("\n  🍀 Good Luck!\n")
  const website = 'https://freenem.com'

  const browser = await puppeteer.launch({ headless: true, ignoreHTTPSErrors: true, args: [
      '--disable-setuid-sandbox',
      '--no-sandbox',
      '--user-agent="Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36"',
      '--lang=en'
    ]
  })
  const page = await browser.newPage()
  await page.setJavaScriptEnabled(true)
  await page.setUserAgent('Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36')
  await page.setViewport({ width: 1866, height: 768})

  console.log(dateToPrint() + '----------------- ATTEMPTING CREATE ACCOUNT -----------------')

  try {

    await page.goto(website, { waitUntil: 'networkidle2', timeout: 0 })
    await sleep(200)
    console.log(website)

    const registerNav = await select(page).getElement('a:contains(Register)')
    await registerNav.click()
    await sleep(2000)

    // There is a bug with puppeeter with visible. See [https://github.com/puppeteer/puppeteer/issues/4356]
    await page.waitForSelector('.register-wrapper input[name=email]', { visible: true })
    await page.waitForSelector('.register-wrapper input[name=password]', { visible: true })
    await page.waitForSelector('.register-wrapper input[name=confirm-password]', { visible: true })
    console.log('All Selector found')

    await page.type('.register-wrapper input[name=email]', email, { delay: 150 })
    await sleep(5000)  // Simulate human who is searching a password
    await page.type('.register-wrapper input[name=password]', pass, { delay: 230 })
    await page.type('.register-wrapper input[name=confirm-password]', pass, { delay: 200 })

    const element = await select(page).getElement('button:contains(REGISTER!)')
    await element.click()
    try {
      // To avoid timeout -> use networkidle2
      await page.waitForNavigation({ waitUntil: 'networkidle2', timeout: 6000 });
      console.log("  👍 SUCCESS! Account created, check your mail to validate your account\n\n")
    } catch (e) {
      // Get inner HTML
      const errorText = await page.evaluate(() => document.querySelector('.register-wrapper .error').innerText)
      console.log('  👎 FAIL. An error occurred', errorText);
    }

  } catch(e) {
    console.log('Error was encountered on: ', website)
    console.error("Error: " + e.message)
    console.log("  👎 FAIL. Account not created. ❌\n\n")
  }

  await page.close()
  await browser.close()
  console.log('----------------- DONE -----------------')
})()
